/*******************************************************************************
 * Copyright 2017 Troy Peterson
 * Maple Scot Development
 *
 * This software is provided under the Creative Commons Attribution 3.0 License
 *
 * You may freely copy and redistribute this code as well as adapt or modify it
 * in any way for any purpose including commercial use.
 *
 * If you use any part of this source code you are required to provide credit
 * to the original author (Troy Peterson) and indicate any changes that were
 * made.
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.maple.scot.digitrecogniser.nn;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.*;
import com.maple.scot.digitrecogniser.functions.findLargest;
import com.maple.scot.digitrecogniser.functions.sigmoid;
import com.maple.scot.digitrecogniser.functions.sigmoidDerivative;
import com.maple.scot.digitrecogniser.interfaces.DoubleUnaryOperator;
import com.maple.scot.digitrecogniser.interfaces.ToIntFunction;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * Main Neural Net class
 * Created by Troy on 02/03/2017.
 */
public class NeuroController {
    private final String TAG = this.getClass().toString();

    private final int inputNeurons;
    private final int outputNeurons;
    private final int hiddenNeurons;
    private double learnRate = 0.04; // Rho
    private DoubleUnaryOperator fActivate; // Function to activate neuron
    private DoubleUnaryOperator fBackProp; // Function to activate neuron
    private ToIntFunction<double[]> fWinner; // Function to find the winner (index)

    // Neuron States
    private final double[] neuronI, neuronO, neuronH; // Generisize this into an number of layers

    // NeuronBiases
    private final double[] biasH;
    private final double[] biasO;

    // Synapses
    private final double[][] synapseIH, synapseHO;


    public NeuroController(int inputNeurons, int outputNeurons, int hiddenNeurons) {
        this.inputNeurons = inputNeurons;
        this.outputNeurons = outputNeurons;
        this.hiddenNeurons = hiddenNeurons;

        neuronI = new double[inputNeurons];
        neuronO = new double[outputNeurons];
        neuronH = new double[hiddenNeurons];


        synapseIH = new double[inputNeurons][hiddenNeurons];
        synapseHO = new double[hiddenNeurons][outputNeurons];


        biasH = randomDoubles(hiddenNeurons);
        biasO = randomDoubles(outputNeurons);

        for (int i=0; i<inputNeurons; i++)
            synapseIH[i] = randomDoubles(hiddenNeurons);

        for (int i=0; i<hiddenNeurons; i++)
            synapseHO[i] = randomDoubles(outputNeurons);

        fActivate = new sigmoid();
        fWinner = new findLargest();
        fBackProp = new sigmoidDerivative();

    }

    private double[] randomDoubles(int count) {
        double[] doubles = new double[count];
        for (int i=0; i< count; i++) {
            doubles[i] = Math.random() - 0.5f;
        }
        return doubles;
    }

    private NeuroController(double[][] ih, double[][] ho, double[] biasH, double[] biasO) {
        this.inputNeurons = ih.length;
        this.outputNeurons = biasO.length;
        this.hiddenNeurons = biasH.length;
        neuronI = new double[inputNeurons];
        neuronO = new double[outputNeurons];
        neuronH = new double[hiddenNeurons];
        this.synapseIH = ih;
        this.synapseHO = ho;
        this.biasH = biasH;
        this.biasO = biasO;
        fActivate = new sigmoid();
        fWinner = new findLargest();
        fBackProp = new sigmoidDerivative();
    }


    public void setLearnRate(double learnRate) {
        this.learnRate = learnRate;
    }

    public double getLearnRate() {
        return learnRate;
    }

    public void stimulate(int neuron, double value) {
        neuronI[neuron] = value;
    }

    public void stimulate(double[] values) {
        System.arraycopy(values, 0, neuronI,0,inputNeurons);
    }

    public int run() {
        feedForward();
        return fWinner.applyAsInt(neuronO);
    }

    public double train(int action) {
        double target[] = new double[outputNeurons];

        target[action] = 1.0;
        feedForward();
        backPropagate(target);


        // Calculate the Mean Squared Error (mse)
        double err = 0.0f;
        for (int i = 0; i < outputNeurons; i++) {
            double e = target[i] - neuronO[i];
            err += (e*e);
        }

        return (0.5f*err);
    }

    private void feedForward() {
        double sum = 0.0;

        // Calculate Input to hidden layer
        for (int h = 0; h < hiddenNeurons; h++) {
            sum = 0.0;
            for (int i = 0; i<inputNeurons; i++)
                sum += neuronI[i] * synapseIH[i][h]; // Value = input Neuron * weighting

            sum += biasH[h];
            neuronH[h] = fActivate.applyAsDouble(sum);
        }

        // Calculate Hidden to Output layer
        for (int o = 0; o < outputNeurons; o++) {
            sum = 0.0;
            for (int h = 0; h < hiddenNeurons; h++)
                sum += neuronH[h] * synapseHO[h][o];

            sum +=  biasO[o];
            neuronO[o] = fActivate.applyAsDouble(sum);
        }

    }

    /**
     * Given a current state of the network and a target output state we work backwards through the network
     * to adjust it towards the expected target output.
     *
     * @param target
     */
    private void backPropagate(double[] target)
    {
        double errorH[] = new double[hiddenNeurons];
        double errorO[] = new double[outputNeurons];

        // Calculate the error factor for the output neurons
        for (int o = 0; o < outputNeurons; o++)
            errorO[o] = (target[o] - neuronO[o]) * fBackProp.applyAsDouble(neuronO[o]);

        // Calculate the error factor for the hidden neurons
        for (int h = 0; h < hiddenNeurons; h++) {
            errorH[h] = 0.0;
            for (int o = 0; o< outputNeurons; o++)
                errorH[h] += errorO[o] * synapseHO[h][o];
            errorH[h] *= fBackProp.applyAsDouble(neuronH[h]);
        }

        // Update weights for the Hidden -> output layer
        for (int o = 0; o < outputNeurons; o++) {
            for (int h = 0; h < hiddenNeurons; h++)
                synapseHO[h][o] += (learnRate * errorO[o] * neuronH[h]);
            // Update the bias
            biasO[o] += (learnRate * errorO[o]);
        }

        // Update weights for the Input -> Hidden layer
        for (int h = 0; h < hiddenNeurons; h++) {
            for (int i = 0; i < inputNeurons; i++)
                synapseIH[i][h] += (learnRate * errorH[h] * neuronI[i]);
            //  Update the bias
            biasH[h] += (learnRate * errorH[h]);
        }

    }


    public void writeTo(FileHandle file) throws IOException {
        Json json = new Json();
        Deflater deflater = new Deflater();
        json.setTypeName(null);
        json.setUsePrototypes(false);
        json.setIgnoreUnknownFields(true);
        json.setOutputType(JsonWriter.OutputType.json);

        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("in", inputNeurons);
        dataMap.put("out", outputNeurons);
        dataMap.put("hid", hiddenNeurons);

        dataMap.put("biasH", encodeDoubleArray(biasH,deflater));
        dataMap.put("biasO", encodeDoubleArray(biasO, deflater));
        String[] ih = new String[inputNeurons];
        for (int i = 0; i < inputNeurons; i++) {
            ih[i] = encodeDoubleArray(synapseIH[i], deflater);
        }
        dataMap.put("synapseIH", ih);

        String[] ho = new String[hiddenNeurons];
        for (int i = 0; i < hiddenNeurons; i++) {
            ho[i] = encodeDoubleArray(synapseHO[i], deflater);
        }
        dataMap.put("synapseHO", ho);
        Gdx.app.log(TAG, (String) dataMap.get("biasH"));

        file.writeString(json.prettyPrint(dataMap), false);

    }

    public static NeuroController load(FileHandle file) {
        Json json = new Json();
        Inflater inflater = new Inflater();
        json.setTypeName(null);
        json.setUsePrototypes(false);
        json.setIgnoreUnknownFields(true);
        json.setOutputType(JsonWriter.OutputType.json);
        HashMap dataMap = json.fromJson(HashMap.class, file.readString());
        final int i, o, h;
        i = ((JsonValue) dataMap.get("in")).getInt(0);
        o = ((JsonValue) dataMap.get("out")).getInt(0);
        h = ((JsonValue) dataMap.get("hid")).getInt(0);

        final double[] biasH, biasO;
        final double[][] ih = new double[i][];
        final double[][] ho = new double[h][];
        try {
            biasH = decodeDoubleArray(((JsonValue)dataMap.get("biasH")).getString(0), inflater, h);
            biasO = decodeDoubleArray(((JsonValue)dataMap.get("biasO")).getString(0), inflater, o);

            for (int c =0; c < i; c++) {
                String s = (String) ((Array)dataMap.get("synapseIH")).get(c);
                ih[c] = decodeDoubleArray(s, inflater, h);
            }

            for (int c =0; c < h; c++) {
                String s = (String) ((Array)dataMap.get("synapseHO")).get(c);
                ho[c] = decodeDoubleArray(s, inflater, o);
            }

            return new NeuroController(ih, ho, biasH, biasO);
        } catch (DataFormatException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getInputNeurons() {
        return inputNeurons;
    }

    public int getOutputNeurons() {
        return outputNeurons;
    }

    public int getHiddenNeurons() {
        return hiddenNeurons;
    }

    private static double[] decodeDoubleArray(String s, Inflater inflater, int size) throws DataFormatException {
        byte[] bytes = Base64Coder.decode(s);
        ByteArray byteArray = new ByteArray(bytes);
        inflater.setInput(byteArray.toArray());

        final byte[] writeBuffer = new byte[size * 8];
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(writeBuffer.length);

        while (!inflater.finished()) {
            int count = inflater.inflate(writeBuffer); // returns the generated btye count.
            outputStream.write(writeBuffer, 0, count);
        }

        inflater.reset();
        int c = 0;
        byte[] in = outputStream.toByteArray();
        double[] out = new double[size];
        for (int i = 0;i<size*8; i+=8 ) {
            double d = ByteBuffer.wrap(in,i, 8 ).getDouble();
            out[c++] = d;
        }

        return out;
    }

    private String encodeDoubleArray(double[] d, Deflater deflater) {
        ByteArray byteArray = new ByteArray();
        for (int i = 0;i<d.length; i++ ) {
            byteArray.addAll(toByteArray(d[i]));
        }
        deflater.setInput(byteArray.toArray());
        final byte[] writeBuffer = new byte[byteArray.size];
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(writeBuffer.length);
        deflater.finish();
        while (!deflater.finished()) {
            int count = deflater.deflate(writeBuffer); // returns the generated btye count.
            outputStream.write(writeBuffer, 0, count);
        }
        deflater.reset();
        return String.valueOf(Base64Coder.encode(outputStream.toByteArray()));
    }

    private byte[] toByteArray(double value) {
        byte[] bytes = new byte[8];
        ByteBuffer.wrap(bytes).putDouble(value);
        return bytes;
    }


}
