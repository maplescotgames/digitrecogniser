/*******************************************************************************
 * Copyright 2017 Troy Peterson
 * Maple Scot Development
 *
 * This software is provided under the Creative Commons Attribution 3.0 License
 *
 * You may freely copy and redistribute this code as well as adapt or modify it
 * in any way for any purpose including commercial use.
 *
 * If you use any part of this source code you are required to provide credit
 * to the original author (Troy Peterson) and indicate any changes that were
 * made.
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.maple.scot.digitrecogniser.functions;


import com.maple.scot.digitrecogniser.interfaces.DoubleUnaryOperator;

/**
 * The derivative of a sigmoid curve.
 * Created by troy on 05/03/17.
 */
public class sigmoidDerivative implements DoubleUnaryOperator {
    @Override
    public double applyAsDouble(double aDouble) {
        return (aDouble * (1.0 - aDouble));
    }
}
