/*******************************************************************************
 * Copyright 2017 Troy Peterson
 * Maple Scot Development
 *
 * This software is provided under the Creative Commons Attribution 3.0 License
 *
 * You may freely copy and redistribute this code as well as adapt or modify it
 * in any way for any purpose including commercial use.
 *
 * If you use any part of this source code you are required to provide credit
 * to the original author (Troy Peterson) and indicate any changes that were
 * made.
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.maple.scot.digitrecogniser.functions;


import com.maple.scot.digitrecogniser.interfaces.ToIntFunction;

/**
 * Find the index of the largest double in the array
 * Created by troy on 05/03/17.
 */
public class findLargest implements ToIntFunction<double[]> {

    @Override
    public int applyAsInt(double[] doubles) {
        int i = 0;
        double maxVal = Double.MIN_VALUE;
        for (int x = 0; x< doubles.length; x++) {
            if (doubles[x] > maxVal) {
                maxVal = doubles[x];
                i = x;
            }
        }
        return i;
    }
}
