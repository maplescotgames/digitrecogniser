/*******************************************************************************
 * Copyright 2017 Troy Peterson
 * Maple Scot Development
 *
 * This software is provided under the Creative Commons Attribution 3.0 License
 *
 * You may freely copy and redistribute this code as well as adapt or modify it
 * in any way for any purpose including commercial use.
 *
 * If you use any part of this source code you are required to provide credit
 * to the original author (Troy Peterson) and indicate any changes that were
 * made.
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.maple.scot.digitrecogniser;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.maple.scot.digitrecogniser.ui.MainScreen;
import com.maple.scot.digitrecogniser.ui.TrainingScreen;

public class DigitRecogniserMain extends Game {
	private static final String SKIN_FILENAME = "images/glassy-ui.json";
	private SpriteBatch batch;
	private Screen trainingScreen;
	private Screen mainScreen;
	private Viewport viewport;
	private Stage stage;
	private Skin skin;


	
	@Override
	public void create () {
		batch = new SpriteBatch();
		viewport = new FillViewport(1280, 720);
		skin = new Skin(Gdx.files.internal(SKIN_FILENAME));

		stage = new Stage(viewport, batch);
		mainScreen = new MainScreen();
		trainingScreen = new TrainingScreen();
		Color bg =  new Color(0f,0.1f,0.3f,1f);

		((MainScreen) mainScreen).setGame(this);
		((MainScreen) mainScreen).setBackgroundColor(bg);
		((MainScreen) mainScreen).setStage(stage);
		((MainScreen) mainScreen).setTrainingScreen(trainingScreen);
		((MainScreen) mainScreen).setSkin(skin);

		((TrainingScreen)trainingScreen).setMainScreen(mainScreen);
		((TrainingScreen)trainingScreen).setGame(this);
		((TrainingScreen)trainingScreen).setSkin(skin);
		((TrainingScreen)trainingScreen).setStage(stage);
		((TrainingScreen)trainingScreen).setBackgroundColor(bg);

		setScreen(mainScreen);
	}


	
	@Override
	public void dispose () {
		trainingScreen.dispose();
		mainScreen.dispose();
		stage.dispose();
		batch.dispose();
		skin.dispose();
	}
}
