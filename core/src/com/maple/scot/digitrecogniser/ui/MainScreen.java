/*******************************************************************************
 * Copyright 2017 Troy Peterson
 * Maple Scot Development
 *
 * This software is provided under the Creative Commons Attribution 3.0 License
 *
 * You may freely copy and redistribute this code as well as adapt or modify it
 * in any way for any purpose including commercial use.
 *
 * If you use any part of this source code you are required to provide credit
 * to the original author (Troy Peterson) and indicate any changes that were
 * made.
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.maple.scot.digitrecogniser.ui;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.maple.scot.digitrecogniser.nn.NeuroController;
import com.badlogic.gdx.Screen;


import java.io.IOException;
import java.nio.IntBuffer;

public class MainScreen implements Screen {
	private static final String TAG = MainScreen.class.toString();
	private static final String DEFAULT_FILENAME = "network.net";

	private Color backgroundColor = null;
	private Stage stage = null;
	private NeuroController brain;
	private Screen trainingScreen = null;

	private FileHandle filename;
	private Game game;

	// Actors
	private DrawCanvas drawArea;
	private Label resultLabel;
	private Label notesLabel;
	private Dialog newDialog;

	private Skin skin;

	@Override
	public void show() {
		buildStage();
	}


	public MainScreen() {
		// If a default file exists, load it on startup.
		if (Gdx.files.internal("data/default.net").exists()) {
			filename =  Gdx.files.internal("data/default.net");
			doLoad();
		}
		filename =  Gdx.files.absolute(Gdx.files.getExternalStoragePath() + "/" + DEFAULT_FILENAME);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {

	}

	private void buildStage() {
		Gdx.app.log(TAG, "Building Stage");
		stage.clear();
		Table top = new Table();
		top.setFillParent(true);
		top.align(Align.left);
		Table leftTable = new Table();

		drawArea = new DrawCanvas(Color.WHITE, Color.BLUE, 512, 512,10);
		top.left();
		leftTable.align(Align.left);
		leftTable.add(drawArea).row();
		Button clearButton = new TextButton("clear", skin, "small" );
		Button testButton = new TextButton("test", skin, "small" );
		leftTable.add(clearButton);

		top.add(leftTable);

		Table rightTable = new Table();
		Table resultTable = new Table();
		resultTable.setBackground(skin.getDrawable("textfield"));
		resultLabel = new Label("?", skin, "bigBlack");
		resultTable.add(resultLabel).pad(20);
		rightTable.add(resultTable).padBottom(10).row();
		rightTable.add(testButton).padBottom(50).row();

		Button trainButton = new TextButton("training", skin, "small" );
		Button loadButton = new TextButton("load Network", skin, "small" );
		Button saveButton = new TextButton("save Network", skin, "small" );
		Table buttonTable = new Table();
		buttonTable.add(trainButton).pad(20);
		buttonTable.add(loadButton).pad(20);
		buttonTable.add(saveButton).pad(20);
		rightTable.add(buttonTable).padBottom(50).row();
		Button newButton = new TextButton("New Network",skin, "small");
		rightTable.row();
		rightTable.add(newButton).padBottom(20).row();
		notesLabel = new Label("", skin);

		rightTable.add(notesLabel);
		rightTable.center();

		top.add(rightTable).expandX();
		buildNewDialog();

		newButton.addListener(new ClickListener() {
            @Override
            public void clicked (InputEvent event, float x, float y) {
                newDialog.show(stage);
            }
        } );
		testButton.addListener(new ClickListener(){
		    @Override
            public void clicked (InputEvent event, float x, float y) {
		        doRecognize();
				drawArea.erase();
            }
        });

		clearButton.addListener(new ClickListener(){
			@Override
			public void clicked (InputEvent event, float x, float y) {
				drawArea.erase();
			}
		});

		trainButton.addListener(new ClickListener(){
			@Override
			public void clicked (InputEvent event, float x, float y) {
				((TrainingScreen) trainingScreen).setNeuroController(brain);
				game.setScreen(trainingScreen);
			}
		});
		saveButton.addListener(new ClickListener(){
			@Override
			public void clicked (InputEvent event, float x, float y) {
				doSave();
			}
		});

		loadButton.addListener(new ClickListener(){
			@Override
			public void clicked (InputEvent event, float x, float y) {
				doLoad();
			}
		});

		Gdx.input.setInputProcessor(stage);
		stage.addActor(top);
	}

	private void buildNewDialog() {
        newDialog = new Dialog("New Network", skin);
        newDialog.setModal(true);
        newDialog.getContentTable().add(new Label("Hidden Nuerons: ", skin, "black"));
        final TextField hiddenCountField = new TextField(String.valueOf(brain.getHiddenNeurons()), skin);
        hiddenCountField.setTextFieldFilter(new TextField.TextFieldFilter.DigitsOnlyFilter());
        newDialog.getContentTable().add(hiddenCountField);
        Button cancel = new TextButton("Cancel", skin, "small");
        Button ok = new TextButton("New", skin, "small");
        newDialog.getButtonTable().add(cancel).padLeft(25);
        newDialog.getButtonTable().add(ok).padRight(25);
        cancel.addListener(new ClickListener(){
            @Override
            public void clicked (InputEvent event, float x, float y) {
                newDialog.hide();
            }
        });

        ok.addListener(new ClickListener(){
            @Override
            public void clicked (InputEvent event, float x, float y) {
                brain = new NeuroController(1024,10,  Integer.valueOf(hiddenCountField.getText()));
                ((TrainingScreen) trainingScreen).setNeuroController(brain);
                newDialog.hide();
            }
        });


    }

    private void doRecognize() {

		long t1 = System.currentTimeMillis();
		Pixmap sourcePM = drawArea.getImage();
	    Pixmap targetPM = new Pixmap(32,32, Pixmap.Format.RGBA8888);
        targetPM.drawPixmap (sourcePM, 0,0, sourcePM.getWidth(), sourcePM.getHeight(), 0, 0, 32,32);

        //drawArea.setDrawable(new TextureRegionDrawable(new TextureRegion(new Texture(targetPM))));
		IntBuffer buffer = targetPM.getPixels().asIntBuffer();


        buffer.rewind();
        int i =0;
        while (buffer.hasRemaining()) {
            int val = buffer.get();
            Color c = new Color(val);
            float stimval = (1.0f - c.r);
            //Gdx.app.log(TAG, "Blue: " + stimval);
            brain.stimulate(i,stimval);
            i++;
        }

		Gdx.app.log(TAG, "Copied image buffer in " + (System.currentTimeMillis() - t1) + "ms");
		t1 = System.currentTimeMillis();

        int r = brain.run();
        notesLabel.setText("Image recognition in " + (System.currentTimeMillis() - t1) + "ms");

        resultLabel.setText(String.valueOf(r));
    }

    private void doSave() {
		notesLabel.setText("Saving Neural Network ");
		long t1 = System.currentTimeMillis();
		FileHandle fileHandle = Gdx.files.absolute(Gdx.files.getExternalStoragePath() + "/test.net");
		try {
			brain.writeTo(fileHandle);
			notesLabel.setText("Network saved in " + (System.currentTimeMillis() - t1) + "ms");
		} catch (IOException e) {
			notesLabel.setText("Error Saving Neural Network: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private void doLoad() {
		if (notesLabel!=null) notesLabel.setText("Loading Neural Network ");
		long t1 = System.currentTimeMillis();
		brain = NeuroController.load(filename);
        if (notesLabel!=null) notesLabel.setText("Network loaded in " + (System.currentTimeMillis() - t1) + "ms");
	}

	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public void setBrain(NeuroController brain) {
		this.brain = brain;
	}

	public void setTrainingScreen(Screen trainingScreen) {
		this.trainingScreen = trainingScreen;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public void setSkin(Skin skin) {
		this.skin = skin;
	}
}
