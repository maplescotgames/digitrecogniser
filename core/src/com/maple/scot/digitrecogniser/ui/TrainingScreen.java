/*******************************************************************************
 * Copyright 2017 Troy Peterson
 * Maple Scot Development
 *
 * This software is provided under the Creative Commons Attribution 3.0 License
 *
 * You may freely copy and redistribute this code as well as adapt or modify it
 * in any way for any purpose including commercial use.
 *
 * If you use any part of this source code you are required to provide credit
 * to the original author (Troy Peterson) and indicate any changes that were
 * made.
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.maple.scot.digitrecogniser.ui;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.*;
import com.maple.scot.digitrecogniser.nn.NeuroController;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;


/**
 *
 * Created by Troy on 09/03/2017.
 */
public class TrainingScreen implements Screen {
    private static final String TAG = TrainingScreen.class.toString();
    private static final String DEFAULT_FILENAME = "samples.trn";
    private Color backgroundColor;
    private Stage stage;
    private NeuroController neuroController;

    private int currDigit=0;
    private Array<Array<Pixmap>> samples = new Array<>(10);
    private boolean isTraining = false;
    private int currCycle = 0;
    private int totalCycles = 1000;
    private float lrStart = 0.2f;
    private float lrFinish = 0.01f;
    private Interpolation interpolation = Interpolation.linear;
    private static double mse = 5;
    private static int currTrainDigit = 0;
    private static int currTrainSample = 0;
    private FileHandle filename;
    private ExecutorService service = Executors.newSingleThreadExecutor();
    private float elapsedTime = 0;
    private Game game;
    private Screen mainScreen;


    //Actors
    private DrawCanvas drawArea;
    private ScrollPane scrollPane;
    private Table sampleTable;
    private ButtonGroup sampleGroup;
    private ProgressBar progressBar;
    private Label trainingText;
    private Label trainLearnRateText;
    private Label timeLabel;
    private Button dialogButton;


    private Skin skin;

    public TrainingScreen() {
        samples = new Array<>();

        // / If a default file exists, load it on startup.
        if (Gdx.files.internal("data/" + DEFAULT_FILENAME).exists()) {
            filename =  Gdx.files.internal("data/" + DEFAULT_FILENAME);
            doLoad();
        } else {
            for (int i =0; i<10; i++)
                samples.add(new Array<Pixmap>());
        }
        filename =  Gdx.files.absolute(Gdx.files.getExternalStoragePath() + "/" + DEFAULT_FILENAME);
    }

    @Override
    public void show() {
        buildStage();
    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (isTraining) {
            elapsedTime+=delta;
            trainingText.setText("Cycle " + currCycle + " / " + totalCycles + "   MSE: " + String.format("%.5f", mse));
            progressBar.setValue((float) currCycle / (float) totalCycles);
            trainLearnRateText.setText("Learn Rate: " + String.format("%1.3f", neuroController.getLearnRate()));
            long seconds  = MathUtils.round(elapsedTime);
            String time = String.format(Locale.ENGLISH, "%02d:%02d",
                    TimeUnit.SECONDS.toMinutes(seconds),
                    TimeUnit.SECONDS.toSeconds(seconds) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(seconds))
            );
            timeLabel.setText("Elapsed: " + time);
        }
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    private void buildStage() {
        currDigit = 0;
        stage.clear();
        Table topTable = new Table();
        topTable.align(Align.left);
        topTable.setFillParent(true);
        Table leftTable = new Table();
        final SelectBox<Integer> digitSelect = new SelectBox<>(skin);
        digitSelect.setItems(0,1,2,3,4,5,6,7,8,9);
        digitSelect.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                currDigit = digitSelect.getSelected();
                updateScrollPane();
            }
        });

        Table selectTable = new Table();
        selectTable.add(new Label("Digit: ", skin));
        selectTable.add(digitSelect);
        leftTable.add(selectTable).padBottom(10).row();
        drawArea = new DrawCanvas(Color.WHITE, Color.BLUE, 512, 512,10);
        leftTable.add(drawArea).row();
        Button clearBtn =new TextButton("clear", skin, "small");
        leftTable.add(clearBtn).padBottom(40).row();
        clearBtn.addListener(new ClickListener(){
            @Override
            public void clicked (InputEvent event, float x, float y) {
                drawArea.erase();
            }
        });
        Table retTable = new Table();
        TextButton retBtn = new TextButton("<- Back", skin, "small");
        retBtn.addListener(new ClickListener(){
            @Override
            public void clicked (InputEvent event, float x, float y) {
                game.setScreen(mainScreen);
            }
        });
        retTable.left();
        retTable.add(retBtn).left();
        leftTable.add(retTable).fillX();
        Table rightTable = new Table();
        rightTable.top();


        rightTable.add(new Label("Samples:",skin, "big")).row();
        sampleTable = new Table();
        sampleGroup = new ButtonGroup();
        scrollPane = new ScrollPane(sampleTable,skin);

        updateScrollPane();

        scrollPane.setHeight(150);
        scrollPane.setScrollBarPositions(true,false);
        scrollPane.setForceScroll(true,false);
        rightTable.add(scrollPane).fillX();
        rightTable.row();
        Table addRemoveTable = new Table();
        Button addBtn = new TextButton("Add Sample", skin, "small");
        addBtn.addListener(new ClickListener(){
            @Override
            public void clicked (InputEvent event, float x, float y) {
                addSample();
            }
        });
        addRemoveTable.add(addBtn);
        sampleGroup.uncheckAll();
        Button removeBtn = new TextButton("Delete Sample", skin, "small");
        removeBtn.addListener(new ClickListener(){
            @Override
            public void clicked (InputEvent event, float x, float y) {
                deleteSample();
            }
        });
        addRemoveTable.add(removeBtn);
        rightTable.add(addRemoveTable).fillX().padBottom(5).row();
        Table loadSaveTable = new Table();
        Button loadButton = new TextButton("Load", skin, "small");
        Button saveButton = new TextButton("Save", skin, "small");
        saveButton.addListener(new ClickListener(){
            @Override
            public void clicked (InputEvent event, float x, float y) {
                saveDialog();
            }
        });
        loadButton.addListener(new ClickListener(){
            @Override
            public void clicked (InputEvent event, float x, float y) {
                loadDialog();
            }
        });
        loadSaveTable.add(saveButton);
        loadSaveTable.add(loadButton);
        rightTable.add(loadSaveTable).fill().padBottom(75).row();
        Table trnTable = new Table();

        trnTable.bottom();
        trnTable.add(new Label("Learn Rate:", skin));
        Table lrTable = new Table();
        lrTable.add(new Label("start", skin));
        lrTable.add(new Label("finish", skin));
        lrTable.add(new Label("interpolation", skin)).row();
        final TextField lrStartTxt = new TextField(String.valueOf(lrStart), skin);
        final TextField lrEndTxt = new TextField(String.valueOf(lrFinish), skin);
        final SelectBox<String> interpBox= new SelectBox<>(skin);

        interpBox.setItems("Linear", "CircleIn", "CircleOut", "SineIn", "SineOut", "ExpIn", "ExptOut");

        lrTable.add(lrStartTxt).width(70);
        lrTable.add(lrEndTxt).width(70);
        lrTable.add(interpBox).width(120);
        trnTable.add(lrTable).padRight(5);
        lrEndTxt.setTextFieldFilter(new FloatFilter());
        lrStartTxt.setTextFieldFilter(new FloatFilter());

        lrStartTxt.setMaxLength(5);
        lrEndTxt.setMaxLength(5);

        lrStartTxt.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (lrStartTxt.getText().trim().length() == 0) return;
                try {
                    lrStart = Float.valueOf(lrStartTxt.getText().trim());
                } catch (Exception e) {
                    lrStart = 0.2f;
                    lrStartTxt.setText(String.valueOf(lrStart));
                }
                if (lrStart <0) lrStart  = 0.2f;

            }

        });

        lrEndTxt.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (lrEndTxt.getText().trim().length() == 0) return;
                try {
                    lrFinish = Float.valueOf(lrEndTxt.getText().trim());
                } catch (Exception e) {
                    lrFinish = 0.01f;
                    lrStartTxt.setText(String.valueOf(lrFinish));
                }
                if (lrFinish <0) lrFinish  =0.01f;
            }

        });

        interpBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                switch (interpBox.getSelected()) {
                    case "CircleIn": interpolation = Interpolation.circleIn;
                        break;
                    case "CircleOut": interpolation = Interpolation.circleOut;
                        break;
                    case "SineIn": interpolation = Interpolation.sineIn;
                        break;
                    case "SineOut": interpolation = Interpolation.sineOut;
                        break;
                    case "ExpIn": interpolation = new Interpolation.PowIn(2);
                        break;
                    case "ExpOut": interpolation = new Interpolation.PowOut(2);
                        break;
                    case "Linear": interpolation = Interpolation.linear;
                }
            }
        });

        trnTable.add(new Label("Cycles:", skin));
        final TextField cycles = new TextField(String.valueOf(totalCycles), skin);
        cycles.setMaxLength(5);
                cycles.setTextFieldFilter(new TextField.TextFieldFilter.DigitsOnlyFilter());
        cycles.setOnlyFontChars(true);

        cycles.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (cycles.getText().contains("\n")) {
                    cycles.setText(cycles.getText().trim());
                }
                if (cycles.getText().trim().length() == 0) return;
                try {
                    totalCycles = Integer.valueOf(cycles.getText().trim());
                } catch (Exception e) {
                    totalCycles = 1;
                    cycles.setText(String.valueOf(1));
                }
                if (totalCycles <1) totalCycles  =1;

            }

        });
        trnTable.add(cycles).width(100).padLeft(10);
        Button train = new TextButton("Train Now", skin, "small");
        trnTable.add(train);
        rightTable.add(trnTable).center();
        train.addListener(new ClickListener(){
            @Override
            public void clicked (InputEvent event, float x, float y) {
                doTraining();
            }
        });

        topTable.add(leftTable).fillX();
        topTable.add(rightTable).expandX();
        stage.addActor(topTable);

    }

    private void updateScrollPane() {
        sampleTable.clearChildren();
        sampleGroup.clear();
        //sampleTable.row().width(50);
        int i = 0;
        for (Pixmap p : samples.get(currDigit)) {
            Sprite img = new Sprite(new Texture(p));
            Sprite selected = new Sprite(img);
            selected.setColor(Color.DARK_GRAY);
            Drawable d = new SpriteDrawable(img);
            Drawable checked = new SpriteDrawable(selected);
            Button b = new ImageButton(d, checked, checked );
            b.setName(String.valueOf(i));
            sampleGroup.add(b);
            sampleTable.add(b).pad(2);
            i++;
        }
        scrollPane.setWidth(100);
        sampleGroup.uncheckAll();


    }

    private void addSample()
    {
        Pixmap sourcePM = drawArea.getImage();
        if (sourcePM == null || sourcePM.getWidth() < 10) return;
        Pixmap targetPM = new Pixmap(32,32, Pixmap.Format.RGBA8888);
        targetPM.drawPixmap (sourcePM, 0,0, sourcePM.getWidth(), sourcePM.getHeight(), 0, 0, 32,32);
        samples.get(currDigit).add(targetPM);
        drawArea.erase();
        updateScrollPane();
    }

    private void deleteSample() {
        if (sampleGroup.getChecked() == null) return;
        Integer sampleNum = Integer.valueOf(sampleGroup.getChecked().getName());
        samples.get(currDigit).removeIndex(sampleNum);
        updateScrollPane();
        sampleGroup.uncheckAll();
    }


    private void doTraining() {
        currCycle = 0;
        elapsedTime = 0;

        final Dialog d = new Dialog("Training", skin);
        neuroController.setLearnRate(lrStart);
        Table dtable = d.getContentTable();

        dtable.add(new Label("Training... ", skin, "black")).row();
        trainLearnRateText = new Label("Learn Rate: " + String.valueOf(lrStart), skin, "black");
        dtable.add(trainLearnRateText).row();
        progressBar = new ProgressBar(0, 1, 0.005f, false, skin);
        trainingText = new Label("                                                      " ,skin, "black" );
        dtable.add(progressBar).row();
        dtable.add(trainingText).row();
        timeLabel = new Label("Elapsed: 0:00",skin, "black");
        dtable.add(trainingText).row();
        dtable.add(timeLabel).padBottom(10);

        dialogButton =  new TextButton("Cancel", skin, "small");
        dialogButton.addListener(new ClickListener(){
            @Override
            public void clicked (InputEvent event, float x, float y) {
                isTraining = false;
                d.hide();
            }
        });
        d.getButtonTable().center().add(dialogButton);

        d.setModal(true);
        d.show(stage);
        isTraining = true;
        currTrainDigit = 0;
        currTrainSample = 0;
        service.submit(new TrainingThread());
    }



    private class TrainingThread implements Runnable {

        @Override
        public void run() {
        while (isTraining) {
                trainSample();
                currTrainSample++;

                if (currTrainSample >= samples.get(currTrainDigit).size) {
                    currTrainSample = 0;
                    currTrainDigit++;
                    if (currTrainDigit == 10) {
                        currTrainDigit = 0;
                        currCycle++;
                        neuroController.setLearnRate(interpolation.apply(lrStart, lrFinish, ((float)currCycle) / totalCycles));
                        Gdx.app.log(TAG, "Learning @ " + neuroController.getLearnRate());
                        if (currCycle > totalCycles) {
                            isTraining = false;
                            ((TextButton) dialogButton).setText("Done");
                        }
                    }
                }
            }
        }

        private void trainSample() {
            //long t1 = System.currentTimeMillis();
            //Gdx.app.log(TAG, "Training Digit " + currTrainDigit + ", sample " + currTrainSample + ". Cycle: " + currCycle);
            if (samples.get(currTrainDigit) == null || samples.get(currTrainDigit).size < 1) return;
            Pixmap sample = samples.get(currTrainDigit).get(currTrainSample);
            if (sample == null) return;
            IntBuffer buffer = sample.getPixels().asIntBuffer();

            buffer.rewind();
            int i = 0;
            while (buffer.hasRemaining()) {
                int val = buffer.get();
                Color col = new Color(val);
                float stimval = (1.0f - col.r);
                neuroController.stimulate(i, stimval);
                i++;
            }

            mse = neuroController.train(currTrainDigit);
        }
    }

    private void saveDialog() {
        FileChooser saveDialog = FileChooser.createSaveDialog("Save Training Samples",skin, filename);
        saveDialog.setResultListener(new FileChooser.ResultListener() {

            @Override
            public boolean result(boolean success, FileHandle result) {
                if(success){
                    filename = result;
                    doSave();
                }
                return true;
            }
        });
        saveDialog.show(stage);
    }

    private void loadDialog() {
        FileChooser saveDialog = FileChooser.createLoadDialog("Load Training Samples", skin, filename);
        saveDialog.setResultListener(new FileChooser.ResultListener() {

            @Override
            public boolean result(boolean success, FileHandle result) {
                if(success){
                    filename = result;
                    doLoad();
                    updateScrollPane();
                }
                return true;
            }
        });
        saveDialog.show(stage);

    }

    private void doSave() {
        Json json = new Json();
        json.setTypeName(null);
        json.setUsePrototypes(false);
        json.setIgnoreUnknownFields(true);
        json.setOutputType(JsonWriter.OutputType.json);


        HashMap<Integer, String[]> map = new HashMap<>();
        int i =0;
        for (Array<Pixmap> a : samples) {
            if (a.size > 0) {
                Array<String> arr = new Array<>();
                for (Pixmap p : samples.get(i)) {
                    try {
                        arr.add(PixSerializer.getAsString(p));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                map.put(i, arr.toArray());
            }
            i++;
        }

        Writer writer = null;
        try {
            writer = filename.writer(false, "UTF-8");
            writer.append(json.toJson(map));
        } catch (Exception ex) {
            throw new SerializationException("Error writing file: " + filename, ex);
        } finally {
            StreamUtils.closeQuietly(writer);
        }
    }


    public void setNeuroController(NeuroController neuroController) {
        this.neuroController = neuroController;
    }

    private void doLoad() {
        // first reset samples;
        samples.clear();
        for (int i =0; i<10; i++)
            samples.add(new Array<Pixmap>());
        currDigit = 0;
        Json json = new Json();
        json.setTypeName(null);
        json.setUsePrototypes(false);
        json.setIgnoreUnknownFields(true);
        json.setOutputType(JsonWriter.OutputType.json);

        try {
            HashMap map = json.fromJson(HashMap.class, filename.readString());
            for (Object e : map.keySet()) {
                int digit = Integer.valueOf(e.toString());
                Array arr = (Array) map.get(e);
                for (Object s : arr) {
                    JsonValue v = (JsonValue) s;
                    samples.get(digit).add(PixSerializer.getAsPixmap(v.getString("value")));
                }
            }
        } catch (Exception e) {
            Gdx.app.error(TAG, "Could not load training data: " + e.toString());
            Gdx.app.error(TAG, e.getMessage());
        }



    }

    /**
     * Will serialize as a Base64 (text) encoded string from a deflate (zip) compressed stream of the pixel values.
     */
    private static final class PixSerializer {

         public static String getAsString(Pixmap p) throws IOException {
             Deflater deflater = new Deflater();
             ByteBuffer pixelBuf = p.getPixels();
             pixelBuf.position(0);
             pixelBuf.limit(pixelBuf.capacity());

             final byte[] writeBuffer = new byte[pixelBuf.capacity()];
             final byte[] readBuffer = new byte[pixelBuf.capacity()];

             pixelBuf.get(readBuffer, 0,  pixelBuf.capacity() ); // Get bytes from pixelBuffer into ReadBuffer
             deflater.setInput(readBuffer); // Set deflater input to ReadBuffer
             ByteArrayOutputStream outputStream = new ByteArrayOutputStream(writeBuffer.length);

             deflater.finish();
             while (!deflater.finished()) {
                 int count = deflater.deflate(writeBuffer); // returns the generated btye count.
                 outputStream.write(writeBuffer, 0, count);
             }
             outputStream.close();
             byte[] output = outputStream.toByteArray();

             String result = String.valueOf(Base64Coder.encode(output));

             pixelBuf.position(0);
             pixelBuf.limit(pixelBuf.capacity());
             return result;
         }

         public static Pixmap getAsPixmap(String s) throws DataFormatException, IOException {
             Inflater inflater = new Inflater();
             byte[] inputBytes = Base64Coder.decode(s);
             inflater.setInput(inputBytes);
             Pixmap pixmap = new Pixmap(32, 32, Pixmap.Format.RGBA8888);
             ByteBuffer pixelBuf = pixmap.getPixels();
             pixelBuf.position(0);
             pixelBuf.limit(pixelBuf.capacity());

             final byte[] readBuffer = new byte[pixelBuf.capacity()]; // Bytes inflated from source stream
             ByteArrayOutputStream outputStream = new ByteArrayOutputStream(readBuffer.length);


             while (!inflater.finished()) {
                 int count = inflater.inflate(readBuffer);
                 outputStream.write(readBuffer, 0, count);
             }

             outputStream.close();

             InputStream in = new DataInputStream(new ByteArrayInputStream( outputStream.toByteArray()));
             int readBytes = 0;
             while ((readBytes = in.read(readBuffer)) > 0) {
                 pixelBuf.put(readBuffer, 0, readBytes);
             }

             pixelBuf.position(0);
             pixelBuf.limit(pixelBuf.capacity());

             return pixmap;
         }
    }

    private class FloatFilter implements TextField.TextFieldFilter {
        @Override
        public boolean acceptChar(TextField textField, char c) {
            return !(c == '\n' || c == ' ' || c == 'r') && (Character.isDigit(c) || c == '.');
        }

    }


    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setSkin(Skin skin) {
        this.skin = skin;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void setMainScreen(Screen mainScreen) {
        this.mainScreen = mainScreen;
    }
}
