/*******************************************************************************
 * Copyright 2017 Troy Peterson
 * Maple Scot Development
 *
 * This software is provided under the Creative Commons Attribution 3.0 License
 *
 * You may freely copy and redistribute this code as well as adapt or modify it
 * in any way for any purpose including commercial use.
 *
 * If you use any part of this source code you are required to provide credit
 * to the original author (Troy Peterson) and indicate any changes that were
 * made.
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.maple.scot.digitrecogniser.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ScreenUtils;

/**
 * A simple Drawing area control
 *
 * Created by Troy on 06/03/2017.
 */
public class DrawCanvas extends Image implements Disposable {
    private FrameBuffer fbo;
    private final Color background, pen;
    private Camera camera;
    private ShapeRenderer shapeRenderer;
    private int px = -1,py = -1;
    private int minx=1000, miny=1000, maxx=0,maxy=0;
    private final int penSize;

    public DrawCanvas(Color background, Color pen, int width, int height, int penSize) {
        super();
        this.background = background;
        this.pen = pen;
        this.penSize = penSize;
        setWidth(width);
        setHeight(height);
        erase();
        shapeRenderer = new ShapeRenderer();
        initInput();
    }

    private void initInput() {
        addListener(new ActorGestureListener() {
            public boolean handle (Event e) {
                if (!(e instanceof InputEvent)) return false;
                InputEvent event = (InputEvent)e;
                switch (event.getType()) {
                    case touchDown:
                        px = -1;
                        py = -1;
                        return true;
                    case touchUp:
                        if (event.isTouchFocusCancel()) return false;
                        return true;
                    case touchDragged:
                        dragEvent(event);
                        return true;
                }
                return false;
            }
        });
    }

    public void erase() {
        int width = MathUtils.round(getWidth());
        int height = MathUtils.round(getHeight());
        if (fbo == null) {
            fbo = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, false);
            camera = new OrthographicCamera(width, height);
            camera.translate(width/2, height/2, 0);
        }
        camera.update();
        fbo.begin();
        Gdx.gl.glClearColor(background.r, background.g, background.b,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        fbo.end();
        setDrawable(new TextureRegionDrawable(new TextureRegion(fbo.getColorBufferTexture())));
        minx = width;
        miny = height;
        maxx = 0;
        maxy = 0;
        layout();
    }

    private void dragEvent(InputEvent e) {
        Vector2 v = e.toCoordinates(this, new Vector2(e.getStageX(), e.getStageY()));
        int x = MathUtils.round(v.x);
        int y = MathUtils.round(getHeight() -v.y);
        //int x = Math.round(e.getStageX());
        //int y = Math.round(getHeight() - e.getStageY());

        if (x<minx) minx = x-20;
        if (y<miny) miny = y-20;
        if (x>maxx) maxx = x+20;
        if (y>maxy) maxy = y+20;

        if (minx< 0) minx = 0;
        if (miny< 0) miny = 0;
        if (maxx> getWidth()) maxx = MathUtils.round(getWidth());
        if (maxy> getHeight()) maxy = MathUtils.round(getHeight());


        fbo.begin();
        shapeRenderer.setProjectionMatrix(camera.combined);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(pen);
        if (px==-1) px = x;
        if (py==-1) py = y;

        shapeRenderer.rectLine(px,py, x,y, penSize*2);
        shapeRenderer.circle(x,y,penSize);
        shapeRenderer.end();
        //batch.end();
        fbo.end();
        setDrawable(new TextureRegionDrawable(new TextureRegion(fbo.getColorBufferTexture())));
        px = x;
        py = y;

    }

    public Rectangle getDrawBounds() {
        return new Rectangle(minx, miny, maxx - minx, maxy - miny);
    }

    public Pixmap getImage() {
        if (minx > maxx || miny > maxy) return new Pixmap(1,1, Pixmap.Format.RGBA8888);
        fbo.begin();
        Pixmap pm = ScreenUtils.getFrameBufferPixmap(minx, miny, maxx - minx, maxy - miny);
        fbo.end();
        return pm;
    }

    public Color getBackground() {
        return background;
    }

    @Override
    public float getMinHeight() {
        return getHeight();
    }

    @Override
    public float getMinWidth() {
        return getWidth();
    }

    @Override
    public void dispose() {
        shapeRenderer.dispose();
        fbo.dispose();
    }
}
